function csvToArray(csv){
	var array = [];
	var lines = csv.split(/\r?\n/);
	var column_name = lines[0].split(',');
	for (i = 1; i < lines.length; i++) {
		var splitValues = lines[i].split(',');
		var object = {};
		for (var elements in column_name) {
			if (splitValues[elements][0] == '[' && splitValues[elements][splitValues[elements].length - 1] == [']']) {
				var temp = splitValues[elements].substring(1, splitValues[elements].length-1).split('|');
				console.log(temp);
				var temp_array = [];
				for (var j = 0; j < temp.length; j++)
					temp_array.push(temp[j]);
				object[column_name[elements]] = splitValues[temp_array];
			}
			else
				object[column_name[elements]] = splitValues[elements];
		}
		array.push(object);
	}
	return array;
}