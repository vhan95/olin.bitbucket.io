function compare_lists(list1, list2){
	var num1 = 0;
	var num2 = 0;
	found = false;
	do {
		if(list1[num1] == list2[num2]) {
			found = true;
			break;
		}
		else {
			if (num2 < list2.length - 1)
				num2 = num2 + 1;
			else {
				num1 = num1 + 1;
                num2 = 0;
			}
		}
	} while (num1 < list1.length);
    return found;
}