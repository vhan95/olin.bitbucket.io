function createCell(row, id, class_name, content, onclick) {
	var cell = row.insertCell(-1);
	cell.id = id;
	cell.className = class_name;
	cell.appendChild(content);
	if (onclick.length > 0)
		cell.setAttribute("onclick", onclick);
}
function create_checkboxes(checkbox_list, div_id, div_className, onclick, cb_id, cb_className, tn_className, condition) {
	for(var node in checkbox_list) {
		var form = document.getElementById(div_id);
		var div = document.createElement("div");
		div.className = div_className;
		var checkbox = document.createElement("input");
		checkbox.setAttribute("onclick", onclick + "('" + node + "')");
		checkbox.id = cb_id + "|" + node;
		checkbox.className = cb_className;
		checkbox.type = "checkbox";
		checkbox.value = checkbox_list[node];
		checkbox.checked = condition;
		var label = document.createElement("label");
		label.setAttribute("for", cb_id + "|" + node);
		var textNode = document.createElement("span");
		textNode.id = "checkbox_label|" + node;
		textNode.className = tn_className;
		textNode.appendChild(document.createTextNode(checkbox_list[node]));
		div.appendChild(checkbox);
		div.appendChild(label);
		div.appendChild(textNode);
		form.appendChild(div);
	}
}
function createListItem(id, class_name, text_node) {
	var list_node = document.createElement("LI");
	list_node.appendChild(document.createTextNode(text_node));
	list_node.id = id;
	list_node.className = class_name;
	return list_node;
}