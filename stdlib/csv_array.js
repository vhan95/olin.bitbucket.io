// Converrt csv strings to array of object
function csvToArray(csv){
    var array = [];
	var lines = csv.split(/\r?\n/);
	var column_name = lines[0].split(',');
	for (i = 1; i < lines.length; i++) {
		var splitValues = lines[i].split(',');
		var object = {};
		for (var elements in column_name) {
			if (splitValues[elements][0] == '[' && splitValues[elements][splitValues[elements].length - 1] == [']'])
				object[column_name[elements]] = arraySplit(splitValues[elements]);
			else if (splitValues[elements][0] == '{' && splitValues[elements][splitValues[elements].length - 1] == ['}'])
				object[column_name[elements]] = objectSplit(splitValues[elements]);
			else
				object[column_name[elements]] = splitValues[elements];
		}
		array.push(object);
	}
	return array;
}

function arraySplit(string_process) {
	var temp = string_process.substring(1, string_process.length-1).split('|');
	var temp_array = [];
	for (var j = 0; j < temp.length; j++)
		if (temp[j][0] == '[' && temp[j][temp[j].length - 1] == [']'])
			temp_array.push(arraySplit(temp[j]));
		else if (temp[j][0] == '{' && temp[j][temp[j].length - 1] == ['}'])
			temp_array.push(objectSplit(temp[j]));
		else if(temp[j].length > 0)
			temp_array.push(temp[j]);
	return temp_array;
}

function objectSplit(string_process) {
	var temp = string_process.substring(1, string_process.length-1).split('&');
	var temp_object = {};
	for (var j = 0; j < temp.length; j++) {
		var property = temp[j].split(':');
		if (property[1][0] == '[' && property[1][property[1].length - 1] == [']'])
			temp_object[property[0]] = arraySplit(property[1]);
		else if (property[1][0] == '{' && property[1][property[1].length - 1] == ['}'])
			temp_object[property[0]] = objectSplit(property[1]);
		else
			temp_object[property[0]] = property[1];
	}
	return temp_object;
}