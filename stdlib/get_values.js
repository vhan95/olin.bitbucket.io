function radio_value(name) {
	var radios = document.getElementsByName(name);
	var return_val = null;
	for (var i = 0; i < radios.length; i++)
		if (radios[i].checked == true){
			return_val = radios[i].value;
			break;
		}
	return return_val;
}