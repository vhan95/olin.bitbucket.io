var scrollX;
var scrollY;
function create_popup(header, contentElement) {
    scrollY = window.scrollY;
    scrollX = window.scrollX;
    var popup_container = document.getElementById("popup_container");
    popup_container.setAttribute("style","visibility: visible");
    var popup = document.createElement("div");
    popup.className = "popup";
    popup.id = "popup";
    var h2 = document.createElement("h2");
    h2.innerHTML = header;
    var a = document.createElement("a");
    a.innerHTML = "&times;";
    a.className = "close";
    a.setAttribute("href", "#");
    a.setAttribute("onclick", "close_popup()");
    var content = document.createElement("div");
    content.className = "content";
    content.appendChild(contentElement);
    popup_container.appendChild(popup);
    popup.appendChild(h2);
    popup.appendChild(a);
    popup.appendChild(content);
}

function close_popup() {
    window.scrollY = scrollY;
    window.scrollX = scrollX;
    document.getElementById('popup_container').removeChild(document.getElementById('popup'));
    popup_container.removeAttribute("style");
}