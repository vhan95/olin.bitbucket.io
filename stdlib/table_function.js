function sortTable(table_id, column_num) {
	var table, rows, switching, i, x, y, shouldSwitch;
	table = document.getElementById(table_id);
	switching = true;
	while (switching) {
		switching = false;
		rows = table.getElementsByTagName("TR");
		for (i = 1; i < (rows.length - 1); i++) {
			shouldSwitch = false;
			x = rows[i].getElementsByTagName("TD")[column_num];
			y = rows[i + 1].getElementsByTagName("TD")[column_num];
			if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
				shouldSwitch = true;
				break;
			}
		}
		if (shouldSwitch) {
			rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
			switching = true;
		}
	}
}

function sortTable_row_className(table_id, class_array) {
	var table, rows, switching, i, x, y, shouldSwitch;
	table = document.getElementById(table_id);
	switching = true;
	while (switching) {
		switching = false;
		rows = table.getElementsByTagName("TR");
		for (i = 1; i < (rows.length - 1); i++) {
			shouldSwitch = false;
			var classNamesX = rows[i].className.split(" ");
			var classNamesY = rows[i + 1].className.split(" ");
			var x = -1;
			var y = -1;
			for (var j = 0; j < classNamesX.length; j++)
				if (class_array.indexOf(classNamesX[j]) >= 0) {
					x = class_array.indexOf(classNamesX[j]);
					break;
				}
			for (var j = 0; j < classNamesY.length; j++)
				if (class_array.indexOf(classNamesY[j]) >= 0) {
					y = class_array.indexOf(classNamesY[j]);
					break;
				}
			if (x > y) {
				shouldSwitch = true;
				break;
			}
		}
		if (shouldSwitch) {
			rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
			switching = true;
		}
	}
}