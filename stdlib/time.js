function check_time_available(element_array, current_time) {
	for (var z = 0; z < element_array.length; z++) {
		time_string = element_array[z].innerHTML.split(" - ");
		time1 = time_string[0].replace(":", "");
		time2 = time_string[1].replace(":", "");
		if (current_time >= time1 && current_time <= time2)
			return true;
	}
	return false;
}

function time_interval_converter(time_interval_string) {
	var time_string = time_interval_string.split('-');
	var time_interval = "";
	for (var i = 0; i < time_string.length; i++) {
		time_interval = time_interval + time_string[i].substring(0, 2) + ":" + time_string[i].substring(2, 4);
		if (i < time_string.length - 1)
			time_interval = time_interval + " - "
	}
	return time_interval
}