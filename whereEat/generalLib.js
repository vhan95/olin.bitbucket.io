function day_setter(current_day) {
	toggle_by_id('day_header|' + current_day, ['not_day', 'normal'])
	toggle_elements_class('wh_' + current_day, ['w_h_hide', 'w_h_show']);
	var day_list = document.getElementsByClassName('wd_' + current_day);
	for (var i = 0; i < day_list.length; i++)
		if (day_list[i].innerHTML == "/")
			toggle_by_id('working_days|' + current_day + "|" + day_list[i].id.split("|")[2], ['days_disabled', 'days_enabled']);
}
function reset_all() {
	toggle_elements_class('week_days', ['days_enabled', 'days_disabled']);
	toggle_elements_class('day_header', ['normal', 'not_day']);
	toggle_elements_class('row_shop', ["shops_enabled", "shops_disabled"]);
	toggle_elements_class('w_h', ["w_h_show", "w_h_hide"]);
	for (var i = 0; i < document.getElementsByClassName("choosen_shop").length; i++)
		document.getElementsByClassName("choosen_shop")[i].checked = false;
}
function createTableRow(tableBody, shop, color) {
	var row = tableBody.insertRow(-1);
	row.className = "shops_disabled row_shop";
	row.id = "row_shop|" + shop['id'];
	var click_func = "show_detail('" + shop.id + "')";
	for (var i = 0; i < table_headers.length; i++) {
		if (table_headers[i] == "working_days"){
			for (var j = 0; j < 7; j++) {
				var content;
				if (shop['operate'][days_name[j]].length != 0)
					content = document.createTextNode('/');
				else
					content = document.createTextNode('X');
				createCell(row, table_headers[i] + "|" + days_name[j] + "|" + shop['id'], "week_days wd_" + days_name[j] + " days_disabled clickable", content, click_func);
			}
		}
		else {
			var content;
			var class_name = table_headers[i];
			if (table_headers[i] == "include") {
				var check_box = document.createElement("INPUT");
				check_box.id = "choosen_shop|" + shop['id'];
				check_box.className = "choosen_shop";
				check_box.type = "checkbox";
				content = check_box;
				click_func = "";
			}
			else if (table_headers[i] == "type") {
				content = document.createTextNode(shopType[shop[table_headers[i]]]);
				class_name = class_name + "|" + shop[table_headers[i]];
			}
			else if (table_headers[i] == "working_hours") {
				var time_list = document.createElement("UL");
				time_list.id = "time_list|" + shop['id'];
				time_list.className = "time_list";
				for (var k in days_name) {
					if (shop['operate'][days_name[k]].length == 0)
						time_list.appendChild(createListItem("w_h|" + days_name[k] + "|" + shop['id'], "w_h wh_" + days_name[k] + " w_h_hide", "Closed for the day"));
					else
						for (var l in shop['operate'][days_name[k]])
							time_list.appendChild(createListItem("w_h|" + days_name[k] + "|" + shop['id'] + "|" + l, "w_h wh_" + days_name[k] + " w_h_hide", time_interval_converter(shop['operate'][days_name[k]][l])));
				}
				content = time_list;
			}
			else if (table_headers[i] == "category") {
				var category_string = "";
				for (var z = 0; z < shop['category'].length; z++) {
					category_string = category_string + category_list[shop['category'][z]];
					if (z < shop['category'].length - 1)
						category_string = category_string + ", ";
				}
				content = document.createTextNode(category_string);
			}
			else if (table_headers[i] == "locations")
				content = document.createTextNode(locations[shop['loc']]);
			else {
				content = document.createTextNode(shop[table_headers[i]]);
			}
			if (table_headers[i] == "cost" || table_headers[i] == "category" || table_headers[i] == "desc" || table_headers[i] == "locations")
				class_name = class_name + " hide";
			if (table_headers[i] != "include")
				class_name = class_name + " clickable";
			createCell(row, table_headers[i] + "|" + shop['id'], class_name, content, click_func);
		}
	}
}
function change() {
	reset_all();
	var choosen_day = radio_value('day');
	day_setter(choosen_day);
	var cost_lvl = parseInt(radio_value('cost'));
	var type_cb = document.getElementsByClassName("type_filter");
	var choosen_type = [];
	for (var i = 0; i < type_cb.length; i++)
		if (type_cb[i].checked)
			choosen_type.push(type_cb[i].value);
	var category_cb = document.getElementsByClassName("category_cb");
	var choosen_category = [];
	for (var i = 0; i < category_cb.length; i++)
		if (category_cb[i].checked)
			choosen_category.push(category_cb[i].value);
	var locations_cb = document.getElementsByClassName("locations_cb");
	var choosen_locations = [];
	for (var i = 0; i < locations_cb.length; i++)
		if (locations_cb[i].checked)
			choosen_locations.push(locations_cb[i].value);
	var choosen_time = parseInt(document.getElementById('time_input').value.replace(":", ""));
	var days_convertion = {
		'sunday': 0,
		'monday': 1,
		'tuesday': 2,
		'wednesday': 3,
		'thursday': 4,
		'friday': 5,
		'saturday': 6,
	}
	var table = document.getElementById("shop_table_tbody");
	for (var i = 0, row; row = table.rows[i]; i++) 
		// day, time, type, cost, category, location
		if (row.cells[days_convertion[choosen_day] + 3].innerHTML == '/')
			if (check_time_available(row.cells[10].childNodes[0].getElementsByClassName("wh_" + choosen_day), choosen_time))
				if (choosen_type.indexOf(row.cells[2].innerHTML) >= 0)
					if (parseInt(row.cells[11].innerHTML) <= cost_lvl)
						if (compare_lists(choosen_category, row.cells[12].innerHTML.split(', ')))
							if (choosen_locations.indexOf(row.cells[14].innerHTML) >= 0) {
								row.className = row.className.replace('shops_disabled', 'shops_enabled');
								row.cells[0].childNodes[0].checked = true;	
							}
	sortTable_row_className('body', ['shops_enabled', 'shops_disabled']);
}
function random_generate() {
	document.getElementById('pick_day').setAttribute('style', 'display: none;');
	document.getElementById('result').removeAttribute('style');
	var choosen_shop = document.getElementsByClassName("choosen_shop");
	var include_list = [];
	for (var i = 0; i < choosen_shop.length; i++)
		if (document.getElementById(choosen_shop[i]['id']).checked == true)
			include_list.push(choosen_shop[i]['id'].split('|')[1]);
	document.getElementById('result_show').removeAttribute('style');
	document.getElementById('desc_show').innerHTML = "";
	if (include_list.length == 1)
		if (document.getElementsByClassName('shops_enabled').length > include_list.length)
			document.getElementById('result_show').innerHTML = "You had picked " + document.getElementById("shop_name|" + include_list[0]).innerHTML + ", why ask me?";
		else
			document.getElementById('result_show').innerHTML = "You have only one optiioon, " + document.getElementById("shop_name|" + include_list[0]).innerHTML;
	else if (include_list.length > 1) {
		var choosen_one = include_list[Math.floor(Math.random() * include_list.length)];
		document.getElementById('result_show').innerHTML = document.getElementById("shop_name|" + choosen_one).innerHTML;
		if (parseInt(document.getElementById("cost|" + choosen_one).innerHTML) >= 4)
			document.getElementById('result_show').setAttribute('style', 'background: linear-gradient(to right, orange, yellow, green, cyan, blue, violet); background-size: 50% auto;color: #000;background-clip: text;text-fill-color: transparent;-webkit-background-clip: text;-webkit-text-fill-color: transparent;-webkit-animation: shine 5s linear infinite;animation: shine 5s linear infinite;');
		if (document.getElementById("desc|" + choosen_one).innerHTML != '-')
			document.getElementById('desc_show').innerHTML = document.getElementById("desc|" + choosen_one).innerHTML;
	}
	else
		document.getElementById('result').innerHTML = "Please pick some shops";
}